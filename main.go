package main

import (
	"database/sql"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"bankaccountapi/bankaccountapi"
)

func setupRoute(h *bankaccountapi.Server) *gin.Engine {
	r := gin.Default()
	users := r.Group("/users")
	// users.GET("/", bankaccountapi.UserService.getUserById)
	// users.POST("/", s.Create)

	users.GET("/:id", h.All)
	// users.PUT("/:id", s.Update)
	// users.DELETE("/:id", s.DeleteByID)
	return r
}
func main() {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}
	createTable := `
	CREATE TABLE IF NOT EXISTS todos (
		id SERIAL PRIMARY KEY,
		todo TEXT,
		created_at TIMESTAMP WITHOUT TIME ZONE,
		updated_at TIMESTAMP WITHOUT TIME ZONE
	);
	CREATE TABLE IF NOT EXISTS secrets (
		id SERIAL PRIMARY KEY,
		key TEXT
	);
	`
	if _, err := db.Exec(createTable); err != nil {
		log.Fatal(err)
	}

	s := &Server{
		db: db,
		todoService: &TodoServiceImp{
			db: db,
		},
		secretService: &SecretServiceImp{
			db: db,
		},
	}

	r := setupRoute(s)

	r.Run(":" + os.Getenv("PORT"))
}
