package user

import (
	"database/sql"
)

type Service struct {
	DB *sql.DB
}

type User struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

func (s *Service) All() ([]User, error) {

	return nil, nil
}

func (s *Service) getUserById(id int) error {

	return nil
}

func (s *Service) createUser() (User, error) {

	return User{}, nil
}

func (s *Service) updateUser() (User, error) {

	return User{}, nil
}

func (s *Service) deleteUser() error {

	return nil
}
