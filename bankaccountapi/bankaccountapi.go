package bankaccountapi

import (
	"bankaccountapi/user"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type UserService interface {
	getUserById(id int)
	All() ([]user.User, error)
	createUser(u *user.User)
	updateUser(u *user.User)
	deleteUser(u *user.User)
}
type Server struct {
	userService UserService
}

func (s *Server) All(c *gin.Context) {

}

func (h *Server) getUserById(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	user, err := h.userService.getUserById(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, user)
}
